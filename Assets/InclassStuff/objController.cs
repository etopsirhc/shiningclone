﻿using UnityEngine;
using System.Collections;

public class objController : MonoBehaviour {
	
	Transform tf;
	Rigidbody rb;
	NetworkView nv;
	
	float lastSync;
	float syncDelay;
	float syncTime;
	Vector3 syncStart, syncEnd;
	
	
	
	
	public float speed = 2.5f;
	
	// Use this for initialization
	void Start () {
		tf = gameObject.GetComponent<Transform>();
		rb = gameObject.GetComponent<Rigidbody>();
		nv = gameObject.GetComponent<NetworkView>();
	}
	
	// Update is called once per frame
	void Update () {
		if (nv.isMine){
			if ( Input.GetButton("Vertical") || Input.GetButton("Horizontal")){
				Vector3 movex = Vector3.up * Input.GetAxis("Vertical") *speed * Time.deltaTime;
				Vector3 movey = Vector3.right * Input.GetAxis("Horizontal") *speed * Time.deltaTime;	
				rb.MovePosition ( rb.position +movex+movey);
				rb.velocity = Vector3.zero;
			}	
		}else{
			syncTime += Time.deltaTime;
			rb.position = Vector3.Lerp(syncStart,syncEnd,syncTime/syncDelay);
		}
		if( Input.GetKeyUp(KeyCode.C)){
			Vector3 rColor = new Vector3(Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f));
			nv.RPC("SetColor",RPCMode.All,rColor);
		}
	}
	void OnSerializeNetworkView(BitStream bs,NetworkMessageInfo nfm){
		Vector3 v3temp = Vector3.zero;
		if (bs.isWriting){
			v3temp = rb.position;
			bs.Serialize(ref v3temp);
		}else{
			bs.Serialize(ref v3temp);
			
			syncTime = 0;
			syncDelay = Time.time - lastSync;
			lastSync = Time.time;
			syncStart = rb.position;
			syncEnd = v3temp;
			
		}
	}
	
	[RPC]
	void SetColor(Vector3 v3){
		Color myColor = new Color(v3.x,v3.y,v3.z);
		gameObject.GetComponent<MeshRenderer>().material.color = myColor;
	}
}
