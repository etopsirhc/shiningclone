﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Manager : MonoBehaviour {
	
	public string gameType = "CIS235UNB_Test_Game";
	public string gameName = "Chris's Server";
	public string gameComment = "some random server";
	public Canvas networkObject; 
	public GameObject serverButton;
	public GameObject ball;
	// Use this for initialization
	void Start () {
	
	}
	
	//server 
	public void StartServer(){
		Network.InitializeServer(32,25001,!Network.HavePublicAddress());
		
	}
	
	void OnServerInitialized(){
		Debug.Log("Server initialized.");
		MasterServer.RegisterHost(gameType,gameName,gameComment);
	}
	
	void OnMasterServerEvent(MasterServerEvent masterEvent){
		if (masterEvent == MasterServerEvent.RegistrationSucceeded){
			Debug.Log("Connected to master server.");
			networkObject.enabled = false;
		}else if (masterEvent == MasterServerEvent.HostListReceived){
			Debug.Log("recieved");
			foreach (HostData hd in MasterServer.PollHostList()){
				Debug.Log(hd.gameType + " " + hd.gameName + " " + hd.comment);
				GameObject button = Instantiate(serverButton,Vector3.zero,Quaternion.identity) as GameObject;
				button.GetComponent<Transform>().SetParent(GameObject.Find("ServerList").GetComponent<Transform>());
				button.GetComponentInChildren<Text>().text = hd.gameName;
				connectTemp(button.GetComponent<Button>(),hd);
			}
		}	
	}
	
	public void ConnectToServer(HostData h){
		Network.Connect(h);
		
	}
	
	void OnPlayerConnected(NetworkPlayer player){
		Debug.Log(" player connected from "+player.ipAddress);
		
	}
	
	void connectTemp(Button button,HostData h){
		button.onClick.AddListener(() => ConnectToServer(h));
	}
	
	// client
	public void GetServerList(){
		MasterServer.RequestHostList(gameType);
	}
	
	void OnConnectedToServer(){
		Debug.Log(" connected to server");
		networkObject.enabled = false;
		Network.Instantiate(ball,Vector3.zero,Quaternion.identity,0);
	}
}
