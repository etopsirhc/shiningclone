﻿using UnityEngine;
using System.Collections;

public class inclass8th : MonoBehaviour {

	public Transform tf;
	public SpriteRenderer sr;
	public Vector3 scale;
	

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>();
		sr = GetComponent<SpriteRenderer>();
		scale = tf.localScale;
	}
	
	void OnMouseDrag(){
		Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mouse.z =0;
		tf.position = mouse;
		
	}
	void OnMouseEnter(){
		tf.localScale = scale*1.2f;
		sr.color = Color.grey;
	}
	void OnMouseExit(){
		tf.localScale = scale;
		sr.color = Color.white;
		
	}
	void OnMouseDown(){
		sr.color = Color.red;
		
	}
	void OnMouseUp(){
		sr.color = new Color (0.2f,1f,.5f);
		
	}
	// Update is called once per frame
	void Update () {
		
	}
}
