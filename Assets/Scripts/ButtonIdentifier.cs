﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ButtonIdentifier : MonoBehaviour, IDeselectHandler, ISelectHandler{

	public void OnDeselect(BaseEventData bed){
		MainPanelController.instance.textBox.SetActive(false);
	}
	public void OnSelect(BaseEventData bed){
		MainPanelController.instance.textBox.SetActive(true);
	}
}
