﻿using UnityEngine;
using System.Collections;
using RPG;

public class MessagePanelController : MonoBehaviour {
	
	private static MessagePanelController _instance;
	public static MessagePanelController instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<MessagePanelController>();
			return _instance;
		}
	}	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
