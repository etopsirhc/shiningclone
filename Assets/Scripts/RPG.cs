﻿// Chris Pote
// RPG
// Basic structual classes of an RPG
// 4/24/15 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RPG {
	
	public enum StatusEffect{
		POISONED = 0,
		SLOWED = 1,
		BURNT = 2,
		ASLEEP = 3,
		FROZEN = 4,
		PARALYZED = 5,
		CONFUSED = 6,
		SILENCED = 7,
		DEAD = 8
	};
	public enum Element{
		NONE = 0,
		FIRE = 1,
		WATER = 2,
		WIND = 3,
		BOLT = 4,
		ALL = 5
	};
	
	public enum Target {
		SINGLE,
		GROUP,
		SIDE,
		ALL // also used for none
	};
	
	public enum Location {
		START,
		MAP,
		CASTLE,
		TOWN,
		MAZE,
		BATTLE,
		EQUIP,
		STATUS
	}
	
	[System.Serializable]
	public class Item{
		public string name = "Unnamed Item";
		public string description = "No Description Set.";
		public int value = 0;
		public bool quest = false;
		public Target targetType = Target.ALL;
		
			
	}
	
	[System.Serializable]
	public class Equipment:Item{
		public Target weaponTargetType = Target.SINGLE;
		public enum Type{
			WEAPON, 
			ARMOR,	
			HELMET,	
			SHIELD
		};
		public enum Compatability{
			DAGGER, // all 3 player types can use
			CLUB,	// Hero and Monk
			STAFF,	// Monk and Mage
			SABER,	// Hero and Mage
			WHIP,	// Mage
			Flail,	// Monk
			SWORD	// Hero
		};
	}
	
	[System.Serializable]
	public class Inventory{
		public Item[] items = {null,null,null,null,null,null,null,null,null,null};
		public Equipment[] equipped = {null,null,null,null};
		public Equipment GetWeapon(){
			if (equipped[0] == null){
				return new Equipment();
			}else{
				return equipped[0];
			}
		}
		
		
	}
	
	[System.Serializable]
	public class Character{
		public enum Stat{
			MAXHEALTH = 0,
			MAXMAGIC = 1,
			ATTACK = 2,
			DEFENCE = 3,
			INTELIGENCE = 4,
			AGILITY = 5,
			LUCK = 6
		};
		public string name = "The Unnamed horror";
		public int[] stats;
		public bool[] status;
		public float[] elementalWeakness = {1f,1f,1f,1f,1f,1f}; // 1f = no weakness, <1f = resistance >1f = weak
		public int level = 0;
		public int health = 0;
		public int magic = 0;
		
		public Inventory inventory = new Inventory();
		
		public Character(){
			stats = new int[]{0,0,0,0,0,0,0};
			status = new bool[]{false,false,false,false,false,false,false,false,false};
		}
		
		public void TakeDamage(int amount,Element element){
			if (element == Element.NONE){
				
			}else{
				
			}
		}
	}
	
	[System.Serializable]
	public class Player:Character{
		bool isMainCharacter = false;
		public bool IsMainCharacter(){
			return isMainCharacter;
		}
	}

	public class Enemy:Character{
		
	}
	
	public class Party{
		public List<Character> unit = new List<Character>();
		public bool allDead = false;
		public int expGained = 0;
		public void debug(){
			unit.Add(new Character());
			unit.Add(new Character());
			unit.Add(new Character());
			unit[0].name = "Ruin";
			unit[1].name = "Wolf";
			unit[2].name = "Tsi";
		}
		public Character GetRandom(){
			if (unit.Count == 0){
				return null;
			}
			return unit[Random.Range(0,unit.Count)];
		}
		
	}
	
	[System.Serializable]
	public class PlayerParty:Party{
		public int gold;
		
		//public List<Player> unit;
		
		public PlayerParty(){
			//unit = Party.unit;
		}
		
		
		public void GetMain(ref Player p){
			p = unit[0] as Player;
		}
	}
	
	public class Effect{
		public void DoDamage(int amount,Element element,Character target){
			target.TakeDamage(amount,element);
		}
		public void DoDamage(int amount,Element element,Party target,Target targetType){
			if (targetType == Target.SINGLE){
				DoDamage(amount,element,target.GetRandom());
			}else{
				foreach (Character character in target.unit){
					DoDamage(amount,element,character);
				}
			}
		}
		public void DoDamage(int amount,Element element,Party[] target,Target targetType){
			for (int i=0; i<target.Length;i++){
				DoDamage(amount,element,target[i],targetType);
			}
		}		
	}
	
	public class Battle{
		public Stack playerMoves;
		
		
		
	}
}