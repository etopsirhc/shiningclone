﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using RPG;

public class MainPanelController : MonoBehaviour {
	
	private static MainPanelController _instance;
	public static MainPanelController instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<MainPanelController>();
			return _instance;
		}
	}	
	
	enum ButtonMap {
		TOP,
		BOTTOM,
		LEFT,
		RIGHT
	}	
	
	Location location = Location.START;
	
	public Button topButton;
	Text topText;
	
	public Button bottomButton;
	Text bottomText;
	
	public Button leftButton;
	Text leftText;
	
	public Button rightButton;
	Text rightText;
	
	public GameObject dummyButton;
	public GameObject buttonBox,textBox;
	public EventSystem es;
	// create button presets dictionaries
	Dictionary<string,ButtonInfo> actions = new Dictionary<string, ButtonInfo>();
	
	bool itemSubmenu = false;
	bool otherMenu = false;
	
	void Start () {
		Debug.Log("start");
		dummyButton = GameObject.FindGameObjectWithTag("Dummy");
	
		PartyPanelController.instance.playerParty = new PlayerParty();	
		PartyPanelController.instance.playerParty.debug();
		Debug.Log ( Location.BATTLE.ToString());
	
		// get text boxes from the buttons, just temporary untill animations can be added
		topText = topButton.GetComponentInChildren<Text>();
		bottomText = bottomButton.GetComponentInChildren<Text>();
		leftText = leftButton.GetComponentInChildren<Text>();
		rightText = rightButton.GetComponentInChildren<Text>();		
		Debug.Log("pre dictionary fill");
		
		// setup action dictionaries. likely to be changed to struct later
		actions.Add("BATTLETOP",new ButtonInfo("Attack",() => Attack(ref PartyPanelController.instance.activeCharacter)));
		actions.Add("BATTLEBOTTOM",new ButtonInfo("Defend",() => Defend(ref PartyPanelController.instance.activeCharacter)));
		actions.Add("BATTLERIGHT",new ButtonInfo("Item",() => Item(ref PartyPanelController.instance.activeCharacter)));
		actions.Add("BATTLELEFT",new ButtonInfo("Spell",() => Spell(ref PartyPanelController.instance.activeCharacter)));
		actions.Add("BATTLEMAIN",new ButtonInfo("Run",() => Flee(ref PartyPanelController.instance.activeCharacter)));

		actions.Add("WORLDRIGHT",new ButtonInfo("Item",() => Item()));
		actions.Add("WORLDLEFT",new ButtonInfo("Spell",() => Spell()));
		actions.Add("WORLDBOTTOM",new ButtonInfo("Status",() => Status()));
		
		actions.Add("ITEMTOP",new ButtonInfo("Use",() => Use()));
		actions.Add("ITEMRIGHT",new ButtonInfo("Equip",() => Equip()));
		actions.Add("ITEMLEFT",new ButtonInfo("Give",() => Give()));
		actions.Add("ITEMBOTTOM",new ButtonInfo("Drop",() => Drop()));
		
		actions.Add("TOWNTOP",new ButtonInfo("Enter",() => Enter()));
		actions.Add("MAZETOP",new ButtonInfo("Search",() => Search()));
		Debug.Log("pre update");
		LocationStart(Location.BATTLE);
		buttonBox.SetActive(false);
		
		//EnterBattle();
	}
	
	void Enable(){
		
	}

	// Update is called once per frame
	void Update () {
		if (location == Location.BATTLE){
			if (buttonBox.activeInHierarchy){
				if (Input.GetButtonUp("Cancel")){
					if (itemSubmenu && !otherMenu){
						itemSubmenu = false;
						RemoveListeners();
						AddListeners();
						UpdateText();
					}else{
						PartyPanelController.instance.DecrementSelection(true);
						//buttonBox.SetActive(false);
						//es.SetSelectedGameObject(dummyButton);
					}
				}
				if (Input.GetAxis("Horizontal")!=0 || Input.GetAxis("Vertical")!=0){
					if ( Input.GetAxis("Horizontal") > 0){
						rightButton.Select();
						OnSelect(rightButton);
						
					}
					if ( Input.GetAxis("Horizontal") < 0){
						leftButton.Select();
						OnSelect(leftButton);
						
					}
					if ( Input.GetAxis("Vertical") > 0){
						topButton.Select();
						OnSelect(topButton);
						
					}
					if ( Input.GetAxis("Vertical") < 0){
						bottomButton.Select();
						OnSelect(bottomButton);
						
					}
				}
			}else{
				if (Input.GetButtonUp("Submit")){
					buttonBox.SetActive(true);
				}
			}
		}
		// debug button 
		if ( Input.GetKeyDown(KeyCode.B)){
			EnterBattle();
		}
	}

	void RemoveListeners(){	
		Debug.Log ("removing listeners");
		
			topButton.onClick.RemoveAllListeners();
			rightButton.onClick.RemoveAllListeners();
			leftButton.onClick.RemoveAllListeners();
			bottomButton.onClick.RemoveAllListeners();
		
	}
	
	void AddListeners(){	
		Debug.Log ("adding listeners");
		string loc;
		
		if (location == Location.TOWN || location == Location.MAZE){
			if (itemSubmenu){
				loc = "ITEM";
				topButton.onClick.AddListener(actions[loc+ButtonMap.TOP.ToString()].action);
				rightButton.onClick.AddListener(actions[loc+ButtonMap.RIGHT.ToString()].action);
				leftButton.onClick.AddListener(actions[loc+ButtonMap.LEFT.ToString()].action);
				bottomButton.onClick.AddListener(actions[loc+ButtonMap.BOTTOM.ToString()].action);
			}else{
				loc = "WORLD";
				topButton.onClick.AddListener(actions[location.ToString()+ButtonMap.TOP.ToString()].action);
				rightButton.onClick.AddListener(actions[loc+ButtonMap.RIGHT.ToString()].action);
				leftButton.onClick.AddListener(actions[loc+ButtonMap.LEFT.ToString()].action);
				bottomButton.onClick.AddListener(actions[loc+ButtonMap.BOTTOM.ToString()].action);
			}
		}else if (location == Location.BATTLE){
			Debug.Log (location.ToString());
			
			if (itemSubmenu){
				Debug.Log (location.ToString() + " submenu");
				
				loc = "ITEM";
				topButton.onClick.AddListener(actions[loc+ButtonMap.TOP.ToString()].action);
				rightButton.onClick.AddListener(actions[loc+ButtonMap.RIGHT.ToString()].action);
				leftButton.onClick.AddListener(actions[loc+ButtonMap.LEFT.ToString()].action);
				bottomButton.onClick.AddListener(actions[loc+ButtonMap.BOTTOM.ToString()].action);
			}else{
				if (PartyPanelController.instance.activePlayer.IsMainCharacter()){
					bottomButton.onClick.AddListener(actions[location.ToString()+"MAIN"].action);
				}else{
					bottomButton.onClick.AddListener(actions[location.ToString()+ButtonMap.BOTTOM.ToString()].action);
				}
				rightButton.onClick.AddListener(actions[location.ToString()+ButtonMap.RIGHT.ToString()].action);
				leftButton.onClick.AddListener(actions[location.ToString()+ButtonMap.LEFT.ToString()].action);
				topButton.onClick.AddListener(actions[location.ToString()+ButtonMap.TOP.ToString()].action);
			}
		}
	}
	void UpdateText(){
			Debug.Log ("updating text");
			string loc;
			if (itemSubmenu){
				loc = "ITEM";
				topText.text = (actions[loc+ButtonMap.TOP.ToString()].name);
				rightText.text = (actions[loc+ButtonMap.RIGHT.ToString()].name);
				leftText.text = (actions[loc+ButtonMap.LEFT.ToString()].name);
				bottomText.text = (actions[loc+ButtonMap.BOTTOM.ToString()].name);
			}else if (location == Location.TOWN || location == Location.MAZE){
				loc = "WORLD";
				topText.text = (actions[loc+ButtonMap.TOP.ToString()].name);
				rightText.text = (actions[loc+ButtonMap.RIGHT.ToString()].name);
				leftText.text = (actions[loc+ButtonMap.LEFT.ToString()].name);
				bottomText.text = (actions[loc+ButtonMap.BOTTOM.ToString()].name);
			}else if (location == Location.BATTLE){
				topText.text = (actions[location.ToString()+ButtonMap.TOP.ToString()].name);
				rightText.text = (actions[location.ToString()+ButtonMap.RIGHT.ToString()].name);
				leftText.text = (actions[location.ToString()+ButtonMap.LEFT.ToString()].name);
				bottomText.text = (actions[location.ToString()+ButtonMap.BOTTOM.ToString()].name);
			}
		Debug.Log("buttons should show proper names");
	}
	
	public void LocationStart(Location l){
		location = l;
		AddListeners();
		UpdateText();
	}
	
	public void LocationUpdated(Location l){
		RemoveListeners();		
		location = l;
		AddListeners();
		UpdateText();
	}
	

	// ====== Stuff to do in battle =======
	void Attack(ref Character character){
		Debug.Log (character.name +" will attack");
		PartyPanelController.instance.IncrementSelection(true);
	} 
	void Defend(ref Character character){ // non-main character
		Debug.Log (character.name +" will defend");
	} 
	void Item(ref Character character){
		Debug.Log ("item in battle"+ character.name);
		RemoveListeners();
		itemSubmenu = true;
		AddListeners();
		UpdateText();
	} 
	void Spell(ref Character character){
		Debug.Log (character.name +" will cast");
		
	}
	void Flee(ref Character character){ // main character only
		Debug.Log ("party will flee as "+ character.name +" says");
	} 
	
	// ====== Stuff to do out of battle =======
	
	void Search(){ // maze only
		Debug.Log ("search");
	}
	void Item(){ // also used in the town
		Debug.Log ("item out of battle");
	}
	void Spell(){ // also used in the town
		Debug.Log ("spell out of battle");
	}
	void Status(){ // also used in the town
		Debug.Log ("status");
	}
	void Enter(){ // town only
		Debug.Log ("enter");
	}
	
	// ====== Stuff to do with item out of battle =======
	void Use(){
		Debug.Log ("use");
	}
	void Give(){
		Debug.Log ("give");
	}
	void Equip(){
		Debug.Log ("equip");
	}
	void Drop(){
		Debug.Log ("drop");
	}
	
	public void OnMouseOver(Button b){
		textBox.SetActive(true);
		b.Select();
		OnSelect(b);
	}
	public void OnMouseOut(Button b){
		es.SetSelectedGameObject(dummyButton);
		textBox.SetActive(false);
	}
	public void OnSelect(Button b){
		if (!textBox.activeSelf){
			textBox.SetActive(true);
		}
		Text textBoxText = textBox.GetComponentInChildren<Text>();
		
		Text t = b.GetComponentInChildren<Text>();
		Debug.Log(t.text);

		Debug.Log(textBoxText.text);
		textBoxText.text = t.text;
	}	
	
	void EnterBattle(){
		PartyPanelController.instance.SetFirstAlive();
		buttonBox.SetActive(true);
	}
							
	// helper classes
	public class ButtonInfo{
		public string name = "";
		public UnityAction action = null;
		
		public ButtonInfo(string s,UnityAction ua){
			name = s;
			action = ua;
		}
	}
}
