﻿using UnityEngine;
using System.Collections;
using RPG;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<GameManager>();
			return _instance;
		}
	}
	
	// variables to keep track of
	PlayerParty player;
	Location location = Location.START;
	
	void Awake(){
		DontDestroyOnLoad(_instance);
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ChangeLocation(Location l){
		location = l;
		
		
		
	}
}
