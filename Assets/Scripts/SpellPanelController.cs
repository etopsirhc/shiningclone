﻿using UnityEngine;
using System.Collections;
using RPG;

public class SpellPanelController : MonoBehaviour {

	private static SpellPanelController _instance;
	public static SpellPanelController instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<SpellPanelController>();
			return _instance;
		}
	}
	Location location = Location.START;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void LocationUpdated(Location l){
		location = l;
	}
}
