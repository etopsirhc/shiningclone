﻿using UnityEngine;
using System.Collections;
using RPG;

public class InventoryPanelController : MonoBehaviour {

	private static InventoryPanelController _instance;
	public static InventoryPanelController instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<InventoryPanelController>();
			return _instance;
		}
	}
		
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
}
