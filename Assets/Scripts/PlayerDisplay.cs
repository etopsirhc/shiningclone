﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

	public Text[] display;
	public Color defaultColor;
	public Color hoverColor;
	public Color selectedColor;
	public Image thisImagePane;
	bool selected = false;
	bool selectable = true;
	public int index = -1;
	
	
	void Start(){
		thisImagePane = gameObject.GetComponent<Image>();
	}	
	public void OnPointerEnter(PointerEventData ped) {
		if( !selected){
			thisImagePane.color = hoverColor;
		}
	}
	public void OnPointerExit(PointerEventData ped) {
		if( !selected){
			thisImagePane.color = defaultColor;
		}
	}
	public void OnPointerClick(PointerEventData ped) {
		if (selectable){
			PartyPanelController.instance.SetSelectedPanel(index);
		}
	}
	public void SetSelected(){
		Debug.Log(index.ToString());
		thisImagePane.color = selectedColor;
		selected = true;
	}
	public void Deselect(){
		thisImagePane.color = defaultColor;
		selected = false;
	}
}
