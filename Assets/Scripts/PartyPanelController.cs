﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RPG;

public class PartyPanelController : MonoBehaviour {
	
	private static PartyPanelController _instance;
	public static PartyPanelController instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<PartyPanelController>();
			return _instance;
		}
	}
	public PlayerParty playerParty;
	Location location = Location.START;
	public Character activeCharacter = null;
	public Player activePlayer = null;
	bool playerSelected = false;
	PlayerDisplay[] displays;
	
	int activeIndex = 0;
	// Use this for initialization
	
	
	void Start () {
		displays = gameObject.GetComponentsInChildren<PlayerDisplay>();
		UpdatePanels();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void SetSelectedPanel(int index){
		activeIndex = index;
		playerSelected = true;
		UpdatePanels();
	}
	
	public void UpdatePanels(){
		for (int i =0; i<playerParty.unit.Count;i++){
			if (playerSelected){
				if (i == activeIndex){
					displays[i].SetSelected();
					activeCharacter = playerParty.unit[i];
					activePlayer = playerParty.unit[i] as Player;
				}else{
					displays[i].Deselect();
				}
			}
			displays[i].display[0].text = playerParty.unit[i].name;
			displays[i].display[1].text = playerParty.unit[i].health.ToString();
			displays[i].display[2].text = playerParty.unit[i].magic.ToString();
			displays[i].display[3].text = playerParty.unit[i].level.ToString();
		}
		
	}
	
	public void IncrementSelection(bool battleMoves){
		Debug.Log(activeIndex);
		if (battleMoves){
			switch (activeIndex){
				case 0:
				if (!playerParty.unit[(activeIndex+1)%3].status[(int)StatusEffect.DEAD]){
						activeIndex = (activeIndex+1)%3;
					}else if(!playerParty.unit[(activeIndex+2)%3].status[(int)StatusEffect.DEAD]){
						activeIndex = (activeIndex+2)%3;
					}else{
						// start fight
					}
					break;
				case 1:
					if (!playerParty.unit[(activeIndex+1)%3].status[(int)StatusEffect.DEAD]){
						activeIndex = (activeIndex+1)%3;
					}else{
						// start fight
					}
					break;
				case 2:
						// start fight
					break;
				default:
					Debug.Log ("ERROR IN INCREMENT SWITCH");
					break;
			}
		}else{
			activeIndex = (activeIndex+1)%3;
		}
		UpdatePanels();
		
	}
	public void DecrementSelection(bool battleMoves){
		if (battleMoves){
			switch (activeIndex){
			case 2:
				if (!playerParty.unit[(activeIndex-1)%3].status[(int)StatusEffect.DEAD]){
					activeIndex = (activeIndex-1)%3;
				}else if(!playerParty.unit[(activeIndex-2)%3].status[(int)StatusEffect.DEAD]){
					activeIndex = (activeIndex-2)%3;
				}else{
					// do nothing! they can not undo an attack that has already happened.
				}
				break;
			case 1:
				if (!playerParty.unit[(activeIndex-1)%3].status[(int)StatusEffect.DEAD]){
					activeIndex = (activeIndex-1)%3;
				}else{
					// do nothing! they can not undo an attack that has already happened.
				}
				break;
			case 0:
				// do nothing! they can not undo an attack that has already happened.
				break;
			default:
				Debug.Log ("ERROR IN INCREMENT SWITCH");
				break;
			}
		}else{
			activeIndex = (activeIndex-1)%3;
		}
		UpdatePanels();
	}
	public void SetFirstAlive(){
		for (int i=0; i<3; i++){
			if (!playerParty.unit[activeIndex].status[(int)StatusEffect.DEAD]){
				SetSelectedPanel(i);
				break;
			}   
		}
	}
	
	
	public Player GetActiveCharacter( ){
		return new Player(); 
	}
	public void LocationUpdated(Location l){
		location = l;
	}
	
	
}
